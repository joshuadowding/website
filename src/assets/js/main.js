$(document).ready(function () {
  $("#theme-toggle #dark").hide();
  $(".menu-toggle #close").hide();

  $(".hover-tooltip").tooltip();

  if($(":root").hasClass("theme-light")) {
    $("#theme-toggle #light").hide();
    $("#theme-toggle #dark").show();
  } else if ($(":root").hasClass("theme-dark")) {
    $("#theme-toggle #light").hide();
    $("#theme-toggle #dark").show();
  }

  $("#theme-toggle").click(function () {
    if ($("#theme-toggle #dark").is(":hidden")) {
      $(":root").removeClass("theme-light");
      $(":root").addClass("theme-dark");

      $("#theme-toggle #light").hide();
      $("#theme-toggle #dark").show();
    }
    else {
      $(":root").removeClass("theme-dark");
      $(":root").addClass("theme-light");

      $("#theme-toggle #dark").hide();
      $("#theme-toggle #light").show();
    }
  });

  $(".menu-toggle").click(function () {
    if ($(".menu-toggle #close").is(":hidden")) {
      $(".sidenav").addClass("show");

      $(".menu-toggle #open").hide();
      $(".menu-toggle #close").show();
    }
    else {
      $(".sidenav").removeClass("show");

      $(".menu-toggle #close").hide();
      $(".menu-toggle #open").show();
    }
  });

  $('#commissions .list-item').each(function() {
    $(this).readingTime({
      readingTimeTarget: $(this).find('.reading-time'),
      wordCountTarget: $(this).find('.word-count'),
      remotePath: $(this).data('file'),
      remoteTarget: $(this).data('target'),
      wordsPerMinute: 200, // NOTE: Slightly conservative (avg. 200-250).
      round: true,
      lang: 'en',
      error: function() {
        $(this).find('.reading-time').remove();
      }
    });
  });

  $('article').each(function() {
    $(this).readingTime({
      readingTimeTarget: $(this).find('.reading-time'),
      wordCountTarget: $(this).find('.word-count'),
      remotePath: $(this).data('file'),
      remoteTarget: $(this).data('target'),
      wordsPerMinute: 200, // NOTE: Slightly conservative (avg. 200-250).
      round: true,
      lang: 'en',
      error: function() {
        $(this).find('.reading-time').remove();
      }
    });
  });

  const currentDate = new Date();
  $('#copyright-year').append(currentDate.getFullYear());

  const currentPage = parse_current_url();
  if (currentPage.includes("projects")) {
    $("#nav-link-projects").addClass("selected");
    $("#sidenav-link-projects").addClass("selected");

    var postID;

    $('.carousel-toggle').click(function () {
      postID = $(this).data('post-id');

      if ($('.carousel-wrapper').css('display') == 'none') {
        $('body').css('overflow-y', 'hidden');
        $('.carousel-wrapper').fadeIn(500, function () {
          $(this).addClass('show');

          $('.post-carousel[data-post-id="' + postID + '"]').css('display', 'block');
          $('.post-carousel[data-post-id="' + postID + '"]').slick({
            dots: false,
            infinite: true,
            speed: 300,
            prevArrow: '<button class="slick-prev"><i class="ri-arrow-left-circle-fill"></i></button>',
            nextArrow: '<button class="slick-next"><i class="ri-arrow-right-circle-fill"></i></button>',
            fade: true,
            cssEase: 'linear'
          });
        });
      } else {
        $('body').css('overflow-y', 'auto');
        $('.carousel-wrapper').fadeOut(500, function () {
          $(this).removeClass('show');

          $('.post-carousel[data-post-id="' + postID + '"]').css('display', 'none');
          $('.post-carousel[data-post-id="' + postID + '"]').slick('unslick');
        });
      }
    });

    $('.carousel-close').click(function () {
      $('body').css('overflow-y', 'auto');
      $('.carousel-wrapper').fadeOut(500, function () {
        $(this).removeClass('show');

        $('.post-carousel[data-post-id="' + postID + '"]').css('display', 'none');
        $('.post-carousel[data-post-id="' + postID + '"]').slick('unslick');
      });
    });
  } else {
    $("#nav-link-scraps").addClass("selected");
    $("#sidenav-link-scraps").addClass("selected");
  }
});

$(window).resize(function() {
  if($(window).width() > 768 && $(".sidenav").hasClass("show")) {
    $(".sidenav").removeClass("show");

    $(".menu-toggle #close").hide();
    $(".menu-toggle #open").show();
  }
});

function parse_current_url() {
  return window.location.pathname;
}
