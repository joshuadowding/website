# joshuadowding.github.io
Personal blog and portfolio.

[![Netlify Status](https://api.netlify.com/api/v1/badges/28cee5ff-df72-4c93-b06f-d60adc98fea3/deploy-status)](https://app.netlify.com/sites/jdowding/deploys)

## Installation & Deployment
Install Jekyll and Bundler:
```bash
gem install jekyll bundler
```

Navigate to the top-level project directory and install the dependent gems:
```bash
bundle install
```

Run the following command to serve the website locally:
```bash
bundle exec jekyll serve (optional: --incremental --host 0.0.0.0)
```
